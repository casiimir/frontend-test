import { createRouter, createWebHistory } from "vue-router";

import Index from "../pages/Index.vue";
import Show from "../pages/Show.vue";

const routes = [
  {
    path: "/",
    name: "Index",
    component: Index,
    props: true,
  },
  {
    path: "/:id",
    component: Show,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
